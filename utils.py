__author__ = 'tyrelsouza'
import os
import json


def get_dropbox_dir():
    """
        Windows and Mac get dropox dir for Business or fallback to personal
    """
    if os.name == "nt":
        dropbox_file = os.path.join(os.getenv('APPDATA'), 'Dropbox', 'info.json')
    else:
        dropbox_file = os.path.expanduser("~/.dropbox/info.json")

    with open(dropbox_file) as dbf:
        dbconfig = json.loads(dbf.read())

    if "business" in dbconfig:
        dropbox_dir = dbconfig['business']['path']
    elif "personal" in dbconfig:
        dropbox_dir = dbconfig['personal']['path']
    else:
        dropbox_dir = os.path.expanduser("~")

    return dropbox_dir
